import sys
import os
import copy
import numpy as np
import torch
from torch.autograd.variable import Variable
from torch.utils.data import DataLoader, Dataset, Sampler
import scipy.interpolate as interpolate
import scipy.ndimage.filters as filters

from utils import data_utils
from data_loader import settings 

class H36MDataset(Dataset):
    def __init__(self, path, name, input_type, actions, input_frames, output_frames, dct_cofs, split=0, sample_rate=2):
        """
        read h36m data to get the dct coefficients.
        :param path: path to dataset
        :param name: save name to specific dataset
        :param input_type: input form(rotation or 3d pos)
        :param actions: actions to read
        :param input_frames: past frame length
        :param output_frames: future frame length
        :param dct_cofs: number of dct coeff. used
        :param split: 0 train, 1 test, 2 validation
        :param sample_rate: 2
        """

        path = os.path.join(path, 'h3.6m/')
        all_seqs, dim_ignore, dim_used, data_mean, data_std = load_data(path, name, input_type, actions,
                                                                       input_frames, input_frames + output_frames,
                                                                       split, sample_rate)

        self.data_mean = data_mean
        self.data_std = data_std

        # global translation and global rotation
        if input_type == 'rot':
            dim_used = dim_used[6:]
        self.all_seqs = all_seqs
        self.dim_used = dim_used
        self.dim_ignore = dim_ignore

        all_seqs = all_seqs[:, :, dim_used]
        all_seqs = all_seqs.transpose(0, 2, 1)
        all_seqs = all_seqs.reshape(-1, input_frames + output_frames)
        all_seqs = all_seqs.transpose()

        dct_m_in, _ = data_utils.get_dct_matrix(input_frames + output_frames)
        dct_m_out, _ = data_utils.get_dct_matrix(input_frames + output_frames) 

        # padding the observed sequence so that it has the same length as observed + future sequence
        pad_idx = np.repeat([input_frames - 1], output_frames)
        i_idx = np.append(np.arange(0, input_frames), pad_idx)
        input_dct_seq = np.matmul(dct_m_in[:dct_cofs, :], all_seqs[i_idx, :])
        input_dct_seq = input_dct_seq.transpose().reshape([-1, len(dim_used), dct_cofs])

        output_dct_seq = np.matmul(dct_m_out[:dct_cofs], all_seqs)
        output_dct_seq = output_dct_seq.transpose().reshape([-1, len(dim_used), dct_cofs])

        self.input_dct_seq = input_dct_seq
        self.output_dct_seq = output_dct_seq

    def __len__(self):
        return np.shape(self.input_dct_seq)[0]

    def __getitem__(self, item):
        return self.input_dct_seq[item], self.output_dct_seq[item], self.all_seqs[item]

def load_data(path, name, input_type, actions, input_frames, seq_len, split, sample_rate):
    """
    adapted from
    https://github.com/una-dinosauria/human-motion-prediction/src/data_utils.py#L216

    :param path: path of dataset
    :param name: dataset save name
    :param input_type: input form(rotation or 3d pos) 
    :param actions: string, types of action
    :param input_frames: past frame length
    :param seq_len: past frame length + future frame length
    :param split: 0->train, 1->test, 2->valid
    :param sample_rate: downsample rate
    :return:
    """

    save_path = os.path.join(path, name)
    save_path = os.path.join(save_path, str(input_type) + '_' + str(seq_len))
    os.makedirs(save_path, exist_ok=True)

    data_file_name_list = ['train', 'test', 'valid']
    data_file_name = "%s_%s_%s_%d.npy" % (input_type, data_file_name_list[split], actions, seq_len)

    subs = np.array([[1, 6, 7, 8, 9], [5], [11]])
    subjects = subs[split]

    if data_file_name in os.listdir(save_path): # not empty
        print("loading %s dataset..., type: %s, action: %s, sequence length: %d" 
              % (data_file_name_list[split], input_type, actions, seq_len))
        sampled_seq = np.load(os.path.join(save_path, data_file_name))
    else:
        print("creating %s dataset..., type: %s, action: %s, sequence length: %d"
              % (data_file_name_list[split], input_type, actions, seq_len))
        acts = settings.define_actions_h36m(actions)
        data_path = os.path.join(path, 'dataset')

        sampled_seq = []
        complete_seq = []
        for subj in subjects:
            for action_idx in np.arange(len(acts)):
                action = acts[action_idx]
                if not (subj == 5):
                    for subact in [1, 2]:  # subactions

                        print("Reading subject {0}, action {1}, subaction {2}".format(subj, action, subact))

                        filename = '{0}/S{1}/{2}_{3}.txt'.format(data_path, subj, action, subact)
                        action_sequence = data_utils.readCSVasFloat(filename)

                        even_sequence, even_frames, odd_sequence, odd_frames = down_sample_senquences(action_sequence)
                        
                        if input_type == 'pos':
                            even_sequence = expmap_to_pos(even_sequence, even_frames)
                            odd_sequence = expmap_to_pos(odd_sequence, odd_frames)

                        even_clip_index = sample_clip_index(even_frames, seq_len)
                        even_clips = even_sequence[even_clip_index, :]
                        odd_clip_index = sample_clip_index(odd_frames, seq_len)
                        odd_clips = odd_sequence[odd_clip_index, :]

                        if len(sampled_seq) == 0:
                            sampled_seq = even_clips
                            complete_seq = even_sequence
                        else:
                            sampled_seq = np.concatenate((sampled_seq, even_clips), axis=0)
                            complete_seq = np.append(complete_seq, even_sequence, axis=0)
                else:
                    print("Reading subject {0}, action {1}, subaction {2}".format(subj, action, 1))
                    filename = '{0}/S{1}/{2}_{3}.txt'.format(data_path, subj, action, 1)
                    action_sequence = data_utils.readCSVasFloat(filename)
                    even_sequence1, even_frames1, _, _ = down_sample_senquences(action_sequence)

                    print("Reading subject {0}, action {1}, subaction {2}".format(subj, action, 2))
                    filename = '{0}/S{1}/{2}_{3}.txt'.format(data_path, subj, action, 2)
                    action_sequence = data_utils.readCSVasFloat(filename)
                    even_sequence2, even_frames2, _, _ = down_sample_senquences(action_sequence)

                    if input_type == 'pos':
                        even_sequence1 = expmap_to_pos(even_sequence1, even_frames1)
                        even_sequence2 = expmap_to_pos(even_sequence2, even_frames2)

                    even_clip_index1, even_clip_index2 = find_indices_srnn(even_frames1, even_frames2, seq_len, input_frames=input_frames)
                    even_clips1 = even_sequence1[even_clip_index1, :]
                    even_clips2 = even_sequence2[even_clip_index2, :]
                    if len(sampled_seq) == 0:
                        sampled_seq = even_clips1
                        sampled_seq = np.concatenate((sampled_seq, even_clips2), axis=0)
                        complete_seq = even_sequence1
                        complete_seq = np.append(complete_seq, even_sequence2, axis=0)
                    else:
                        sampled_seq = np.concatenate((sampled_seq, even_clips1), axis=0)
                        sampled_seq = np.concatenate((sampled_seq, even_clips2), axis=0)
                        complete_seq = np.append(complete_seq, even_sequence1, axis=0)
                        complete_seq = np.append(complete_seq, even_sequence2, axis=0)

        np.save(os.path.join(save_path, data_file_name), sampled_seq)
        # if is not testing or validation then get the data statistics
        if not (subj == 5 or subj == 11):
            data_std = np.std(complete_seq, axis=0)
            data_mean = np.mean(complete_seq, axis=0)
            np.save(os.path.join(save_path, 'mean.npy'), data_mean)
            np.save(os.path.join(save_path, 'std.npy'), data_std)

    # load mean and std
    data_mean = np.load(os.path.join(save_path, 'mean.npy'))
    data_std = np.load(os.path.join(save_path, 'std.npy'))

    if input_type == 'rot':
        dimensions_to_use = np.array(
            [6, 7, 8, 9, 12, 13, 14, 15, 21, 22, 23, 24, 27, 28, 29, 30, 36, 37, 38, 39, 40, 41, 42,
             43, 44, 45, 46, 47, 51, 52, 53, 54, 55, 56, 57, 60, 61, 62, 75, 76, 77, 78, 79, 80, 81, 84, 85, 86])
        dimensions_to_ignore = np.array(
            [0, 1, 2, 3, 4, 5, 10, 11, 16, 17, 18, 19, 20, 25, 26, 31, 32, 33, 34, 35, 48, 49, 50, 58,
              59, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 82, 83, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97,
              98])
        # dimensions_to_ignore = []
        # dimensions_to_use = []
        # dimensions_to_ignore.extend(list(np.where(data_std < 1e-4)[0]))
        # dimensions_to_use.extend(list(np.where(data_std >= 1e-4)[0]))
        data_std[dimensions_to_ignore] = 1.0
        data_mean[dimensions_to_ignore] = 0.0
    elif input_type == 'pos':
        joint_to_ignore = np.array([0, 1, 6, 11, 16, 20, 23, 24, 28, 31])
        dimensions_to_ignore = np.concatenate((joint_to_ignore * 3, joint_to_ignore * 3 + 1, joint_to_ignore * 3 + 2))
        dimensions_to_use = np.setdiff1d(np.arange(sampled_seq.shape[2]), dimensions_to_ignore)

    return sampled_seq, dimensions_to_ignore, dimensions_to_use, data_mean, data_std

def down_sample_senquences(full_sequence, down_sample_rate=2):
    n, d = full_sequence.shape
    even_list = range(0, n, down_sample_rate)
    odd_list = range(1, n, down_sample_rate)

    even_sequence = np.array(full_sequence[even_list, :])
    even_frames = len(even_sequence)
    odd_sequence = np.array(full_sequence[odd_list, :])
    odd_frames = len(odd_sequence)

    return even_sequence, even_frames, odd_sequence, odd_frames

def expmap_to_pos(sample_sequence, num_frames):
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    the_seq = Variable(torch.from_numpy(sample_sequence)).float().to(device)
    # remove global rotation and translation
    the_seq[:, 0:6] = 0
    p3d = data_utils.expmap2xyz_torch(the_seq)
    the_sequence = p3d.view(num_frames, -1).cpu().data.numpy()

    return the_sequence

def sample_clip_index(num_frames, seq_len):
    fs = np.arange(0, num_frames - seq_len + 1)
    fs_sel = fs
    for i in np.arange(seq_len - 1):
        fs_sel = np.vstack((fs_sel, fs + i + 1))
    fs_sel = fs_sel.transpose()

    return fs_sel

def find_indices_srnn(frame_num1, frame_num2, seq_len, input_frames=10):
    """
    Adapted from https://github.com/una-dinosauria/human-motion-prediction/blob/master/src/seq2seq_model.py#L478

    which originaly from
    In order to find the same action indices as in SRNN.
    https://github.com/asheshjain399/RNNexp/blob/master/structural_rnn/CRFProblems/H3.6m/processdata.py#L325
    """

    # Used a fixed dummy seed, following
    # https://github.com/asheshjain399/RNNexp/blob/srnn/structural_rnn/forecastTrajectories.py#L29
    SEED = 1234567890
    rng = np.random.RandomState(SEED)

    T1 = frame_num1 - 150
    T2 = frame_num2 - 150  # seq_len
    idxo1 = None
    idxo2 = None
    for _ in np.arange(0, 4):
        idx_ran1 = rng.randint(16, T1)
        idx_ran2 = rng.randint(16, T2)
        idxs1 = np.arange(idx_ran1 + 50 - input_frames, idx_ran1 + 50 - input_frames + seq_len)
        idxs2 = np.arange(idx_ran2 + 50 - input_frames, idx_ran2 + 50 - input_frames + seq_len)
        if idxo1 is None:
            idxo1 = idxs1
            idxo2 = idxs2
        else:
            idxo1 = np.vstack((idxo1, idxs1))
            idxo2 = np.vstack((idxo2, idxs2))
    return idxo1, idxo2

