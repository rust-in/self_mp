import sys
import os
import copy
import numpy as np
import torch
from torch.utils.data import DataLoader, Dataset, Sampler
import scipy.interpolate as interpolate
import scipy.ndimage.filters as filters

from utils import data_utils
from data_loader import settings 

def load_data(path_to_dataset, subjects, actions, sample_rate, seq_len, input_frames=10, data_mean=None, data_std=None):
    """
    adapted from
    https://github.com/una-dinosauria/human-motion-prediction/src/data_utils.py#L216

    :param path_to_dataset: path of dataset
    :param subjects:
    :param actions:
    :param sample_rate:
    :param seq_len: past frame length + future frame length
    :param is_norm: normalize the expmap or not
    :param data_std: standard deviation of the expmap
    :param data_mean: mean of the expmap
    :param input_frames: past frame length
    :return:
    """

    sampled_seq = []
    complete_seq = []
    # actions_all = define_actions("all")
    # one_hot_all = np.eye(len(actions_all))
    for subj in subjects:
        for action_idx in np.arange(len(actions)):
            action = actions[action_idx]
            if not (subj == 5):
                for subact in [1, 2]:  # subactions

                    print("Reading subject {0}, action {1}, subaction {2}".format(subj, action, subact))

                    filename = '{0}/S{1}/{2}_{3}.txt'.format(path_to_dataset, subj, action, subact)
                    action_sequence = data_utils.readCSVasFloat(filename)

                    even_sequence, even_frames, odd_sequence, odd_frames = down_sample_senquences(action_sequence)

                    even_clip_index = sample_clip_index(even_frames, seq_len)
                    even_clips = even_sequence[even_clip_index, :]
                    odd_clip_index = sample_clip_index(odd_frames, seq_len)
                    odd_clips = odd_sequence[odd_clip_index, :]

                    if len(sampled_seq) == 0:
                        sampled_seq = even_clips
                        complete_seq = even_sequence
                    else:
                        sampled_seq = np.concatenate((sampled_seq, even_clips), axis=0)
                        complete_seq = np.append(complete_seq, even_sequence, axis=0)
            else:
                print("Reading subject {0}, action {1}, subaction {2}".format(subj, action, 1))
                filename = '{0}/S{1}/{2}_{3}.txt'.format(path_to_dataset, subj, action, 1)
                action_sequence = data_utils.readCSVasFloat(filename)

                even_sequence1, even_frames1, _, _ = down_sample_senquences(action_sequence)

                print("Reading subject {0}, action {1}, subaction {2}".format(subj, action, 2))
                filename = '{0}/S{1}/{2}_{3}.txt'.format(path_to_dataset, subj, action, 2)
                action_sequence = data_utils.readCSVasFloat(filename)
                even_sequence2, even_frames2, _, _ = down_sample_senquences(action_sequence)

                even_clip_index1, even_clip_index2 = find_indices_srnn(even_frames1, even_frames2, seq_len, input_frames=input_frames)
                even_clips1 = even_sequence1[even_clip_index1, :]
                even_clips2 = even_sequence2[even_clip_index2, :]
                if len(sampled_seq) == 0:
                    sampled_seq = even_clips1
                    sampled_seq = np.concatenate((sampled_seq, even_clips2), axis=0)
                    complete_seq = even_sequence1
                    complete_seq = np.append(complete_seq, even_sequence2, axis=0)
                else:
                    sampled_seq = np.concatenate((sampled_seq, even_clips1), axis=0)
                    sampled_seq = np.concatenate((sampled_seq, even_clips2), axis=0)
                    complete_seq = np.append(complete_seq, even_sequence1, axis=0)
                    complete_seq = np.append(complete_seq, even_sequence2, axis=0)

    # if is not testing or validation then get the data statistics
    if not (subj == 5 and subj == 11):
        data_std = np.std(complete_seq, axis=0)
        data_mean = np.mean(complete_seq, axis=0)

    dimensions_to_ignore = []
    dimensions_to_use = []
    dimensions_to_ignore.extend(list(np.where(data_std < 1e-4)[0]))
    dimensions_to_use.extend(list(np.where(data_std >= 1e-4)[0]))
    data_std[dimensions_to_ignore] = 1.0
    data_mean[dimensions_to_ignore] = 0.0

    return sampled_seq, dimensions_to_ignore, dimensions_to_use, data_mean, data_std

def down_sample_senquences(full_sequence, down_sample_rate=2):
    n, d = full_sequence.shape
    even_list = range(0, n, down_sample_rate)
    odd_list = range(1, n, down_sample_rate)

    even_sequence = np.array(full_sequence[even_list, :])
    even_frames = len(even_sequence)
    odd_sequence = np.array(full_sequence[odd_list, :])
    odd_frames = len(odd_sequence)

    return even_sequence, even_frames, odd_sequence, odd_frames

def sample_clip_index(num_frames, seq_len):
    fs = np.arange(0, num_frames - seq_len + 1)
    fs_sel = fs
    for i in np.arange(seq_len - 1):
        fs_sel = np.vstack((fs_sel, fs + i + 1))
    fs_sel = fs_sel.transpose()

    return fs_sel