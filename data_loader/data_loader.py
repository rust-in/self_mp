import torch
import numpy as np
from torch.utils.data import DataLoader, Dataset, Sampler


def build_dataloader(dataset, batch_size, shuffle=True, num_workers=0, pin_memory=True, drop_last=False):
    if num_workers == 0:
        dataloader = DataLoader(
            dataset, batch_size=batch_size, shuffle=shuffle, pin_memory=pin_memory, drop_last=drop_last)
    else:
        dataloader = DataLoader(
            dataset, batch_size=batch_size, shuffle=shuffle, num_workers=num_workers, pin_memory=pin_memory, drop_last=drop_last)
    return dataloader

class DataSampler(Sampler):
    def __init__(self, data):
        self.indices = data['valid_index']

    def __iter__(self):
        rand_i = np.random.permutation(len(self.indices))
        return iter(self.indices[rand_i])

    def __len__(self):
        return len(self.indices)


def collate_fn(batch):
    input_batch = {}
    target_batch = {}
    input, target = batch[0]
    for k in input.keys():
        input_batch[k] = torch.stack([batch[i][0][k] for i in range(len(batch))])
    for k in target.keys():
        target_batch[k] = torch.stack([batch[i][1][k] for i in range(len(batch))])
    return input_batch, target_batch


# def build_dataloader(dataset, batch_size):
#     sampler = DataSampler(dataset.data)
#     dataloader = DataLoader(
#         dataset, batch_size=batch_size, sampler=sampler,
#         collate_fn=collate_fn, shuffle=False, drop_last=True)
#     return dataloader
