import os
os.environ['OMP_NUM_THREADS'] = "1"
import argparse
import json
import logging
import time
import ruamel.yaml
import pickle as pk
import numpy as np
from addict import Dict
import copy
import pandas as pd
import datetime

import torch
from torch import nn
from torch.autograd import Variable
from torch.utils.data import Subset
import torch.nn.functional as F
from torch.utils.tensorboard import SummaryWriter
from model import ST_Net, get_positional_encoding, get_subsequent_mask
from data_loader import H36MDataset, H36MTransformDataset, build_dataloader
from data_loader.settings import define_actions_h36m, define_actions_cmu
from utils import trainer_utils, data_utils, loss_utils, Optim


class Trainer(object):
    def __init__(self, config):
        self.config = config
        data_config = config.data
        trainer_config = config.trainer
        net_config = config.net
        self.save_name = str(config.trainer.save_name)

        save_root = config.trainer.save_root
        self.save_dir = os.path.join(save_root, self.save_name)
        os.makedirs(self.save_dir, exist_ok=True)
        json.dump(config, indent=4, fp=open(
            os.path.join(self.save_dir, 'config.yml'), 'w'))

        log_root = config.trainer.log_root
        log_dir = os.path.join(log_root, self.save_name)
        os.makedirs(log_dir, exist_ok=True)
        self.writer = SummaryWriter(log_dir)
        self.logger = logging.getLogger('trainer')
        self.set_logger(os.path.join(log_dir, 'train.log'))
        self.logger.info(json.dumps(config, indent=4)) # log configuration information
        self.device = torch.device(
            'cuda:0' if torch.cuda.is_available() else 'cpu')
        
        self.csv_path = os.path.join(self.save_dir, self.save_name + '.csv')

        # create model
        self.model = ST_Net(
            n_joint=net_config.num_joint, n_dim=9 if data_config.type == 'rotmat' else 3,
            n_embed=net_config.embedding_feature, n_head=net_config.attention_head_num,
            d_k=net_config.attention_weight_feature, d_v=net_config.attention_weight_feature,
            d_feed=net_config.feed_forward_feature, n_atten=net_config.attention_block_num,
            dropout=net_config.dropout)
        self.model.to(self.device)
        
        if torch.cuda.is_available():
            self.model = nn.DataParallel(self.model)
        
        # optimizer
        self.start_epoch = 0
        self.err_best = 10000
        self.init_lr = trainer_config.optimizer.lr
        if trainer_config.optimizer.get('type', 'Adam') == 'Adam':
            self.optimizer = Optim.ScheduledOptim(
                torch.optim.Adam(self.model.parameters(),
                    betas=(trainer_config.optimizer.beta1, 
                           trainer_config.optimizer.beta2),
                    eps=trainer_config.optimizer.eps),
                init_lr=self.init_lr, 
                d_model=net_config.embedding_feature,
                n_warmup_steps=trainer_config.optimizer.warmup
            )

        # load saved checkpoint
        if config.get('resume_from'):
            self.logger.info('model resumed from %s' % config.resume_from)
            map_location = None if torch.cuda.is_available() else 'cpu'
            ckpt = torch.load(config.resume_from, map_location=map_location)

            self.start_epoch = ckpt['epoch']
            self.err_best = ckpt['err']
            self.model.load_state_dict(ckpt['state_dict'])
            self.optimizer.load_state_dict(ckpt['optimizer'], ckpt['init_lr'], ckpt['n_steps'])

        # # load train / valid dataset
        if data_config.get('dataset', 'h36m') == 'h36m':
            self.train_dataset = H36MTransformDataset(
                path=data_config.data_dir, name=data_config.name, input_type=data_config.type, 
                actions=data_config.actions, input_frames=trainer_config.input_frames,  
                output_frames=trainer_config.output_frames, split=0)
            self.valid_dataset = H36MTransformDataset(
                path=data_config.data_dir, name=data_config.name, input_type=data_config.type,
                actions=data_config.actions, input_frames=trainer_config.input_frames, 
                output_frames=trainer_config.output_frames, split=2)

        self.train_loader = build_dataloader(self.train_dataset, batch_size=data_config.train_batch, shuffle=True, pin_memory=True)
        self.valid_loader = build_dataloader(self.valid_dataset, batch_size=data_config.train_batch, shuffle=False, pin_memory=True)
        
        # load test dataset
        self.test_loader = dict()
        if data_config.get('dataset', 'h36m') == 'h36m':
            acts = define_actions_h36m('all')
            for act in acts:
                test_dataset = H36MTransformDataset(path=data_config.data_dir, name=data_config.name, 
                                                    input_type=data_config.type, actions=act, 
                                                    input_frames=trainer_config.input_frames, 
                                                    output_frames=trainer_config.output_frames, split=1)
                self.test_loader[act] = build_dataloader(test_dataset, batch_size=data_config.test_batch, shuffle=False, pin_memory=True)

        self.acts = acts # fot test

        self.logger.info("data loaded.")
        self.logger.info("train data length: %d" % len(self.train_dataset))
        self.logger.info("valid data length: %d" % len(self.valid_dataset))


    def run(self):

        data_config = self.config.data
        trainer_config = self.config.trainer
        net_config = self.config.net

        disp_loss = loss_utils.AccumLoss()

        for epoch in range(self.start_epoch, trainer_config.epochs):

            ret_log = np.array([epoch + 1])
            head = np.array(['epoch'])

            # start training
            disp_loss, loss_dict = self.train(epoch, disp_loss)

            ret_log = np.append(ret_log, [self.optimizer._optimizer.param_groups[0]['lr'], loss_dict['loss']])
            head = np.append(head, ['lr', 't_l'])
            if 'rot' in data_config.type:
                ret_log = np.append(ret_log, [loss_dict['eul'], loss_dict['p3d']])
                head = np.append(head, ['t_e', 't_3d'])
                self.logger.info('Epoch: %d, the epoch loss ON TRAINING SET is %f, the euler angle error is %f,'
                                 'the mean pose error is %f' % (epoch, loss_dict['loss'], loss_dict['eul'], loss_dict['p3d']))
                self.writer.add_scalars('criterion/train', {'train_euler': loss_dict['eul'],
                                                            'train_3dpos': loss_dict['p3d']}, epoch)
            else:
                ret_log = np.append(ret_log, [loss_dict['p3d']])
                head = np.append(head, ['t_3d'])
                self.logger.info('Epoch: %d, the epoch loss ON TRAINING SET is %f, the mean pose error is %f' 
                                 % (epoch, loss_dict['loss'], loss_dict['p3d']))
                self.writer.add_scalars('criterion/train', {'train_3dpos': loss_dict['p3d']}, epoch)
        
            # start validating
            val_dict = self.val()

            if 'rot' in data_config.type:
                ret_log = np.append(ret_log, [val_dict['eul'], val_dict['p3d']])
                head = np.append(head, ['v_e', 'v_3d'])
                self.logger.info('Epoch: %d, on VALID SET, the euler angle error is %f,'
                                'the mean pose error is %f' % (epoch, val_dict['eul'], val_dict['p3d']))
                self.writer.add_scalars('criterion/train', {'valid_euler': val_dict['eul'],
                                                            'valid_3dpos': val_dict['p3d']}, epoch)
            else:
                ret_log = np.append(ret_log, [val_dict['p3d']])
                head = np.append(head, ['v_3d'])
                self.logger.info('Epoch: %d, on VALID SET, the mean pose error is %f' % (epoch, val_dict['p3d']))
                self.writer.add_scalars('criterion/train', {'valid_3dpos': val_dict['p3d']}, epoch)

            # start testing
            test_eul_temp, test_eul_head = np.array([]), np.array([])
            test_p3d_temp, test_p3d_head = np.array([]), np.array([])

            for act in self.acts:
                test_dict = self.test(act)

                if 'rot' in data_config.type:
                    test_eul_temp = np.append(test_eul_temp, test_dict['eul']) # append euler error
                    test_eul_head = np.append(test_eul_head, [act + '80', act + '160', act + '320', act + '400', act + '560', act + '1000'])

                test_p3d_temp = np.append(test_p3d_temp, test_dict['p3d']) # append 3dpos error
                test_p3d_head = np.append(test_p3d_head, [act + '3d80', act + '3d160', act + '3d320', act + '3d400', act + '3d560', act + '3d1000'])

                self.logger.info('Epoch: %d, finish testing on %s' % (epoch, act))

            if 'rot' in data_config.type:
                ret_log = np.append(ret_log, test_eul_temp)
                head = np.append(head, test_eul_head)
            ret_log = np.append(ret_log, test_p3d_temp)
            head = np.append(head, test_p3d_head)

            # update log file
            df = pd.DataFrame(np.expand_dims(ret_log, axis=0))
            if epoch == self.start_epoch:
                df.to_csv(self.csv_path, header=head, index=False)
            else:
                with open(self.csv_path, 'a') as f:
                    df.to_csv(f, header=False, index=False)

            # update checkpoint
            val_metric = val_dict['eul'] if data_config.type == 'rot' else val_dict['p3d']
            if not np.isnan(val_metric):
                is_best = val_metric < self.err_best
                self.err_best = min(val_metric, self.err_best)
            else:
                is_best = False 

            save_state = {'epoch': epoch + 1,
                          'err': val_metric,
                          'init_lr': self.init_lr,
                          'n_steps': self.optimizer.n_steps,
                          'state_dict': self.model.state_dict(),
                          'optimizer': self.optimizer._optimizer.state_dict()}
            trainer_utils.save_ckpt(save_state, self.save_dir, is_best, ['best.pth.rar', 'last.pth.rar'])

        self.logger.info(self.save_name)
    
    def train(self, epoch, disp_loss):
        t_l = loss_utils.AccumLoss()
        t_e = loss_utils.AccumLoss()
        t_3d = loss_utils.AccumLoss()

        self.model.train()

        input_type = self.config.data.type

        for batch_num, (inputs, targets, all_seq) in enumerate(self.train_loader):

            inputs = inputs.to(self.device).float()
            targets = targets.to(self.device, non_blocking=True).float()

            outputs = self.model(inputs)
            n = outputs.shape[0] # batch size
            
            # print(outputs)
            # loss = loss_utils.l2_loss(outputs, targets)
            if input_type == 'rot':
                # loss = loss_utils.l2_loss_expmap2pos(outputs, targets)
                loss = loss_utils.l2_loss(outputs, targets)
            elif input_type == 'rotmat':
                # loss = loss_utils.l2_loss_rotmat2pos(outputs, targets)
                # loss = loss_utils.l2_loss(outputs, targets)
                loss = loss_utils.l2_loss_rotmat_normalized(outputs, targets)
            elif input_type == 'pos':
                loss = loss_utils.l2_loss_pos(outputs, targets)
            t_l.update(loss.data.tolist() * n, n)

            self.optimizer.zero_grad()
            loss.backward()
            self.optimizer.step_and_update_lr()

            if input_type == 'rot':
                e_err = loss_utils.euler_error_expmap_transform(outputs, targets)
                t_e.update(e_err.data.tolist() * n, n)
                m_err = loss_utils.mpjpe_error_expmap_transform(outputs, targets) 
            elif input_type == 'rotmat':
                e_err = loss_utils.euler_error_rotmat_transform(outputs, targets)
                t_e.update(e_err.data.tolist() * n, n)
                m_err = loss_utils.mpjpe_error_rotmat_transform(outputs, targets)
            elif input_type == 'pos':
                e_err = None
                m_err = loss_utils.mpjpe_error_pos_transform(outputs, targets)
            t_3d.update(m_err.data.tolist() * n, n)


            disp_loss.update(loss.data.tolist() * n, n)
            disp_iter = self.config.trainer.disp_iter
            self.writer.add_scalar('loss/train', loss.item(), disp_loss.step)
            if disp_loss.step % disp_iter == 0:
                self.logger.info('Epoch: %d, batch: %d / %d, steps: %d, the loss is: %f, lr is: %f.' %
                                    (epoch, batch_num+1, len(self.train_loader), disp_loss.step, 
                                    disp_loss.avg, self.optimizer._optimizer.param_groups[0]['lr']))
                disp_loss.__init__(step=disp_loss.step)
        
            del all_seq, outputs, loss, e_err, m_err

        loss_dict = {}
        loss_dict['loss'] = t_l.avg
        loss_dict['eul'] = t_e.avg
        loss_dict['p3d'] = t_3d.avg

        return disp_loss, loss_dict


    def val(self):

        t_e = loss_utils.AccumLoss()
        t_3d = loss_utils.AccumLoss()

        self.model.eval()

        input_type = self.config.data.type

        for batch_num, (inputs, targets, all_seq) in enumerate(self.valid_loader):

            inputs = inputs.to(self.device).float()
            targets = targets.to(self.device, non_blocking=True).float()

            outputs = self.model(inputs)
            n = outputs.shape[0] # batch size

            if input_type == 'rot':
                e_err = loss_utils.euler_error_expmap_transform(outputs, targets)
                t_e.update(e_err.data.tolist() * n, n)
                m_err = loss_utils.mpjpe_error_expmap_transform(outputs, targets)
            elif input_type == 'rotmat':
                e_err = loss_utils.euler_error_rotmat_transform(outputs, targets)
                t_e.update(e_err.data.tolist() * n, n)
                m_err = loss_utils.mpjpe_error_rotmat_transform(outputs, targets)
            elif input_type == 'pos':
                e_err = None
                m_err = loss_utils.mpjpe_error_pos_transform(outputs, targets)
            t_3d.update(m_err.data.tolist() * n, n)
            
            del all_seq, outputs, e_err, m_err

        val_dict = {}
        val_dict['eul'] = t_e.avg
        val_dict['p3d'] = t_3d.avg
        val_list = np.array([t_e.avg, t_3d.avg])

        return val_dict

    def test(self, act):

        eval_frame = [1, 3, 7, 9, 13, 24] # 80, 160, 320, 400, 560, 1000
        t_e = np.zeros(len(eval_frame))
        t_3d = np.zeros(len(eval_frame))

        self.model.eval()

        input_frames = self.config.trainer.input_frames
        output_frames = self.config.trainer.output_frames
        input_type = self.config.data.type

        for batch_num, (inputs, targets, all_seq) in enumerate(self.test_loader[act]):

            inputs = inputs.to(self.device).float()
            targets = targets.to(self.device, non_blocking=True).float()
            
            for test_iter in range(output_frames):
                outputs = self.model(inputs)
                inputs = Variable(torch.cat((inputs[:, :, 1:, :].data, outputs[:, :, -1:, :].data), 2))
            
            pred_seq = inputs[:, :, -output_frames:, :].contiguous()
            bs, joint_num, seq_len, dim_len = pred_seq.shape

            
            if input_type == 'rot':
                pred_tmp = pred_seq.transpose(1, 2).contiguous().view(-1, 3)
                pred_eul = data_utils.rotmat2euler_torch(data_utils.expmap2rotmat_torch(pred_tmp))
                pred_eul = pred_eul.view(bs, seq_len, -1)
                targ_tmp = targets.transpose(1, 2).contiguous().view(-1, 3)
                targ_eul = data_utils.rotmat2euler_torch(data_utils.expmap2rotmat_torch(targ_tmp))
                targ_eul = targ_eul.view(bs, seq_len, -1)

                top = torch.zeros(3).to(pred_seq.device)
                top = top[(None,) * 3 + (...,)].expand(bs, 2, seq_len, -1)
                pred_tmp = torch.cat((top, pred_seq), 1).transpose(1,2).contiguous().view(bs*seq_len, -1)
                pred_pos = data_utils.expmap2xyz_torch(pred_tmp).view(bs, seq_len, -1, 3)
                targ_tmp = torch.cat((top, targets), 1).transpose(1,2).contiguous().view(bs*seq_len, -1)
                targ_pos = data_utils.expmap2xyz_torch(targ_tmp).view(bs, seq_len, -1, 3)

            elif input_type == 'rotmat':
                pred_tmp = pred_seq.clone().view(-1, dim_len).view(-1, 3, 3)
                pred_tmp = data_utils.rotmat_normalization_torch(pred_tmp)
                pred_eul = data_utils.rotmat2euler_torch(pred_tmp).view(bs, joint_num, seq_len, 3)
                pred_eul = pred_eul.transpose(1,2).contiguous().view(bs, seq_len, -1)
                targ_tmp = targets.view(-1, dim_len).view(-1, 3, 3)
                targ_eul = data_utils.rotmat2euler_torch(targ_tmp).view(bs, joint_num, seq_len, 3)
                targ_eul = targ_eul.transpose(1,2).contiguous().view(bs, seq_len, -1) # bs * seq_len * (joint num * 3)

                eye = torch.eye(3).to(pred_seq.device)
                eye = eye[(None,) * 3 + (...,)].expand(bs, -1, seq_len, -1, -1)
                pred_tmp = pred_seq.clone().view(*pred_seq.shape[:-1], 3, 3)
                pred_tmp = data_utils.rotmat_normalization_torch(pred_tmp)
                pred_tmp = torch.cat((eye, pred_tmp), 1).transpose(1,2).contiguous().view(-1, joint_num+1, 3, 3)
                pred_pos = data_utils.rotmat2xyz_torch(pred_tmp).view(bs, seq_len, -1, 3)
                targ_tmp = targets.view(*targets.shape[:-1], 3, 3)
                targ_tmp = torch.cat((eye, targ_tmp), 1).transpose(1,2).contiguous().view(-1, joint_num+1, 3, 3)
                targ_pos = data_utils.rotmat2xyz_torch(targ_tmp).view(bs, seq_len, -1, 3) # bs * seq_len * joint num + 1 * 3

            elif input_type == 'pos':
                top = torch.zeros(3).to(pred_seq.device)
                top = top[(None,) * 3 + (...,)].expand(bs, -1, seq_len, -1)
                pred_pos = torch.cat((top, pred_seq), 1).transpose(1,2)
                targ_pos = torch.cat((top, targets), 1).transpose(1,2) # bs, seq_len, joint num + 1,  3

            
            for i, frame in enumerate(eval_frame):

                loss_eul=None
                if 'rot' in input_type:

                    loss_eul = torch.mean(torch.sqrt(torch.sum(
                        (pred_eul[:, frame, :] - targ_eul[:, frame, :]) ** 2, dim=-1) + 1e-8))

                    t_e[i] += loss_eul.data.tolist()
                
                loss_p3d = torch.mean(torch.sqrt(torch.sum((
                    pred_pos[:, frame, :, :] - targ_pos[:, frame, :, :]) ** 2, dim=2) + 1e-8))
                t_3d[i] += loss_p3d.data.tolist()

            del all_seq, inputs, targets, outputs, loss_eul, loss_p3d
            
        test_dict = {}
        test_dict['eul'] = t_e
        test_dict['p3d'] = t_3d
        return test_dict


    def set_logger(self, log_file):
        self.logger.setLevel(logging.DEBUG)
        formatter = logging.Formatter(fmt='%(asctime)s: %(message)s')
        stream_handler = logging.StreamHandler()
        stream_handler.setLevel(logging.INFO)
        stream_handler.setFormatter(formatter)
        self.logger.addHandler(stream_handler)
        file_handler = logging.FileHandler(log_file, mode='w+')
        file_handler.setLevel(logging.INFO)
        file_handler.setFormatter(formatter)
        self.logger.addHandler(file_handler) 

def parse_args():
    parser = argparse.ArgumentParser(description='Train a motion generator')
    parser.add_argument(
        'config',
        help='train config file path')
    parser.add_argument(
        '--resume_from',
        help='the checkpoint file to resume from')
    args = parser.parse_args()
    return args

def main():
    # old version
    args = parse_args()
    cfg = Dict(ruamel.yaml.safe_load(open(args.config)))
    if args.resume_from is not None:
        cfg.resume_from = args.resume_from
    trainer = Trainer(cfg)
    trainer.run()

if __name__ == '__main__':
    main()
