import os
import argparse
import json
import logging
import time
import yaml
import pickle as pk
import numpy as np
from addict import Dict
import copy
import pandas as pd

import torch
from torch import nn
from torch.utils.data import Subset
import torch.nn.functional as F
from torch.utils.tensorboard import SummaryWriter

from model import GCN, GCN_mul 
from data_loader.h36m_dataset import H36MDataset
from data_loader.data_loader import build_dataloader
from data_loader.settings import define_actions_h36m, define_actions_cmu
from utils import trainer_utils, loss_utils

class Trainer(object):
    def __init__(self, config):
        self.config = config
        data_config = config.data
        trainer_config = config.trainer
        net_config = config.net
        self.save_name = str(config.trainer.save_name)
        
        save_root = config.trainer.save_root
        self.save_dir = os.path.join(save_root, self.save_name)
        os.makedirs(self.save_dir, exist_ok=True)
        json.dump(config, indent=4, fp=open(
            os.path.join(self.save_dir, 'config.yml'), 'w'))

        log_root = config.trainer.log_root
        log_dir = os.path.join(log_root, self.save_name)
        os.makedirs(log_dir, exist_ok=True)
        self.writer = SummaryWriter(log_dir)
        self.logger = logging.getLogger('trainer')
        self.set_logger(os.path.join(log_dir, 'train.log'))
        self.logger.info(json.dumps(config, indent=4)) # log configuration information
        self.device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

        self.csv_path = os.path.join(self.save_dir, self.save_name + '.csv')

        # create model
        node_n = 48 if data_config.type == 'rot' else 66
        if net_config.gcn_type == 0:
            self.model = GCN(input_feature=trainer_config.dct_cofs, hidden_feature=net_config.hidden_layer_features, 
                            p_dropout=net_config.dropout, num_stage=net_config.hidden_layer_num, node_n=node_n)
        else:
            self.model = GCN_mul(input_feature=trainer_config.dct_cofs, hidden_feature=net_config.hidden_layer_features,
                                 p_dropout=net_config.dropout, num_stage=net_config.hidden_layer_num, node_n=node_n,
                                 expert_model_num=net_config.expert_model_num, gcn_type=net_config.gcn_type, norm_type=net_config.norm_type)
        self.model.to(self.device)

        if torch.cuda.is_available():
            self.model = nn.DataParallel(self.model)

        # optimizer
        self.start_epoch = 0
        self.err_best = 10000
        self.lr_now = trainer_config.optimizer.lr
        if trainer_config.optimizer.get('type', 'Adam') == 'Adam':
            self.optimizer = torch.optim.Adam(self.model.parameters(), lr=trainer_config.optimizer.lr)
        
        # load saved checkpoint
        if config.get('resume_from'):
            self.logger.info('model resumed from %s' % config.resume_from)
            map_location = None if torch.cuda.is_available() else 'cpu'
            ckpt = torch.load(config.resume_from, map_location=map_location)

            self.start_epoch = ckpt['epoch']
            self.err_best = ckpt['err']
            self.lr_now = ckpt['lr']
            self.model.load_state_dict(ckpt['state_dict'])
            self.optimizer.load_state_dict(ckpt['optimizer'])

        # load train / valid dataset
        if data_config.get('dataset', 'h36m') == 'h36m':
            self.train_dataset = H36MDataset(path=data_config.data_dir, name=data_config.name, input_type=data_config.type, 
                                             actions=data_config.actions, input_frames=trainer_config.input_frames,  
                                             output_frames=trainer_config.output_frames, dct_cofs=trainer_config.dct_cofs, split=0)
            self.valid_dataset = H36MDataset(path=data_config.data_dir, name=data_config.name, input_type=data_config.type,
                                             actions=data_config.actions, input_frames=trainer_config.input_frames, 
                                             output_frames=trainer_config.output_frames, dct_cofs=trainer_config.dct_cofs, split=2)

        self.train_loader = build_dataloader(self.train_dataset, batch_size=data_config.train_batch, shuffle=True, pin_memory=True)
        self.valid_loader = build_dataloader(self.valid_dataset, batch_size=data_config.test_batch, shuffle=False, pin_memory=True)
        
        # load test dataset
        self.test_loader = dict()
        if data_config.get('dataset', 'h36m') == 'h36m':
            acts = define_actions_h36m('all')
            for act in acts:
                test_dataset = H36MDataset(path=data_config.data_dir, name=data_config.name, input_type=data_config.type, actions=act, 
                                           input_frames=trainer_config.input_frames, output_frames=trainer_config.output_frames,
                                           dct_cofs=trainer_config.dct_cofs, split=1)
                self.test_loader[act] = build_dataloader(test_dataset, batch_size=data_config.test_batch, shuffle=False, pin_memory=True)
        elif data_config.get('type', 'h36m') in ['cmu', 'cmu3D']:
            acts = define_actions_cmu('all')

        self.acts = acts # fot test

        self.logger.info("data loaded.")
        self.logger.info("train data length: %d" % len(self.train_dataset))
        self.logger.info("valid data length: %d" % len(self.valid_dataset))


        # for i, (inputs, targets, all_seq) in enumerate(valid_loader):
        #     print(inputs, targets, all_seq)
        #     break

    def run(self):

        data_config = self.config.data
        trainer_config = self.config.trainer
        net_config = self.config.net

        disp_loss = loss_utils.AccumLoss()

        for epoch in range(self.start_epoch, trainer_config.epochs):
            # learning rate decay
            if (epoch + 1) % trainer_config.optimizer.lr_decay == 0:
                lr_gamma = trainer_config.optimizer.lr_gamma
                self.lr_now = trainer_utils.lr_decay(self.optimizer, self.lr_now, lr_gamma)
            
            ret_log = np.array([epoch + 1])
            head = np.array(['epoch'])

            disp_loss, loss_dict = self.train(epoch, disp_loss)

            ret_log = np.append(ret_log, [self.lr_now, loss_dict['loss']])
            head = np.append(head, ['lr', 't_l'])
            if data_config.type == 'rot':
                ret_log = np.append(ret_log, [loss_dict['eul'], loss_dict['p3d']])
                head = np.append(head, ['t_e', 't_3d'])
                self.logger.info('Epoch: %d, the epoch loss ON TRAINING SET is %f, the euler angle error is %f,'
                                 'the mean pose error is %f' % (epoch, loss_dict['loss'], loss_dict['eul'], loss_dict['p3d']))
                self.writer.add_scalars('criterion/train', {'train_euler': loss_dict['eul'],
                                                            'train_3dpos': loss_dict['p3d']}, epoch)
            else:
                ret_log = np.append(ret_log, [loss_dict['p3d']])
                head = np.append(head, ['t_3d'])
                self.logger.info('Epoch: %d, the epoch loss ON TRAINING SET is %f, the mean pose error is %f' 
                                 % (epoch, loss_dict['loss'], loss_dict['p3d']))
                self.writer.add_scalars('criterion/train', {'train_3dpos': loss_dict['p3d']}, epoch)
            
            val_dict = self.val()

            if data_config.type == 'rot':
                ret_log = np.append(ret_log, [val_dict['eul'], val_dict['p3d']])
                head = np.append(head, ['v_e', 'v_3d'])
                self.logger.info('Epoch: %d, on VALID SET, the euler angle error is %f,'
                                'the mean pose error is %f' % (epoch, val_dict['eul'], val_dict['p3d']))
                self.writer.add_scalars('criterion/train', {'valid_euler': val_dict['eul'],
                                                            'valid_3dpos': val_dict['p3d']}, epoch)
            else:
                ret_log = np.append(ret_log, [val_dict['p3d']])
                head = np.append(head, ['v_3d'])
                self.logger.info('Epoch: %d, on VALID SET, the mean pose error is %f' % (epoch, val_dict['p3d']))
                self.writer.add_scalars('criterion/train', {'valid_3dpos': val_dict['p3d']}, epoch)

            
            test_eul_temp, test_eul_head = np.array([]), np.array([])
            test_p3d_temp, test_p3d_head = np.array([]), np.array([])

            for act in self.acts:
                test_dict = self.test(act)

                if data_config.type == 'rot':
                    test_eul_temp = np.append(test_eul_temp, test_dict['eul']) # append euler error
                    test_eul_head = np.append(test_eul_head, [act + '80', act + '160', act + '320', act + '400'])

                test_p3d_temp = np.append(test_p3d_temp, test_dict['p3d']) # append 3dpos error
                test_p3d_head = np.append(test_p3d_head, [act + '3d80', act + '3d160', act + '3d320', act + '3d400'])

                if trainer_config.output_frames > 10:
                    if data_config.type == 'rot':
                        test_eul_head = np.append(test_eul_head, [act + '560', act + '1000'])
                    test_p3d_head = np.append(test_p3d_head, [act + '3d560', act + '3d1000'])
                
                self.logger.info('Epoch: %d, finish testing on %s' % (epoch, act))
            
            if data_config.type == 'rot':
                ret_log = np.append(ret_log, test_eul_temp)
                head = np.append(head, test_eul_head)
            ret_log = np.append(ret_log, test_p3d_temp)
            head = np.append(head, test_p3d_head)

            # update log file
            df = pd.DataFrame(np.expand_dims(ret_log, axis=0))
            if epoch == self.start_epoch:
                df.to_csv(self.csv_path, header=head, index=False)
            else:
                with open(self.csv_path, 'a') as f:
                    df.to_csv(f, header=False, index=False)
            # update checkpoint
            val_metric = val_dict['eul'] if data_config.type == 'rot' else val_dict['p3d']
            if not np.isnan(val_metric):
                is_best = val_metric < self.err_best
                self.err_best = min(val_metric, self.err_best)
            else:
                is_best = False 

            save_state = {'epoch': epoch + 1,
                          'lr': self.lr_now,
                          'err': val_metric,
                          'state_dict': self.model.state_dict(),
                          'optimizer': self.optimizer.state_dict()}
            trainer_utils.save_ckpt(save_state, self.save_dir, is_best, ['best.pth.rar', 'last.pth.rar'])

        self.logger.info(self.save_name)
    
    def train(self, epoch, disp_loss):
        t_l = loss_utils.AccumLoss()
        t_e = loss_utils.AccumLoss()
        t_3d = loss_utils.AccumLoss()

        self.model.train()

        dim_used = self.train_dataset.dim_used
        input_frames = self.config.trainer.input_frames
        dct_cofs = self.config.trainer.dct_cofs
        input_type = self.config.data.type

        for batch_num, (inputs, targets, all_seq) in enumerate(self.train_loader):
            # skip the last batch if only have one sample for batch_norm layers
            batch_size = inputs.shape[0]
            if batch_size == 1:
                continue

            inputs = inputs.to(self.device, non_blocking=True).float()
            targets = targets.to(self.device, non_blocking=True).float()
            all_seq = all_seq.to(self.device, non_blocking=True).float()

            outputs = self.model(inputs)
            n = outputs.shape[0] # batch size

            if input_type == 'rot':
                loss = loss_utils.sen_loss(outputs, all_seq, dim_used, dct_cofs)
            else:
                loss = loss_utils.sen_loss_p3d(outputs, all_seq, dim_used, dct_cofs)
            t_l.update(loss.cpu().data.numpy() * n, n)

            self.optimizer.zero_grad()
            loss.backward()
            if self.config.trainer.max_norm:
                nn.utils.clip_grad_norm(self.model.parameters(), max_norm=1)
            self.optimizer.step()

            if input_type == 'rot':
                # angle space error
                e_err = loss_utils.euler_error(outputs, all_seq, input_frames, dim_used, dct_cofs)
                t_e.update(e_err.cpu().data.numpy() * n, n)
                # 3d error
                m_err = loss_utils.mpjpe_error(outputs, all_seq, input_frames, dim_used, dct_cofs)
                t_3d.update(m_err.cpu().data.numpy() * n, n)
            else:
                m_err = loss_utils.mpjpe_error_p3d(outputs, all_seq, input_frames, dim_used, dct_cofs)
                t_3d.update(m_err.cpu().data.numpy() * n, n)
            
            # set logger
            disp_loss.update(loss.cpu().data.numpy() * n, n)
            disp_iter = self.config.trainer.disp_iter
            self.writer.add_scalar('loss/train', loss.item(), disp_loss.step)
            if disp_loss.step % disp_iter == 0:
                    self.logger.info('Epoch: %d, batch: %d / %d, steps: %d, the loss is: %f' %
                                     (epoch, batch_num+1, len(self.train_loader), disp_loss.step, 
                                      disp_loss.avg))
                    disp_loss.__init__(step=disp_loss.step)
        
        loss_dict = {}
        loss_dict['loss'] = t_l.avg
        loss_dict['eul'] = t_e.avg
        loss_dict['p3d'] = t_3d.avg
        # loss_list = np.append(loss_list, [t_e.avg, t_3d.avg])
        return disp_loss, loss_dict
            
    def val(self):

        t_e = loss_utils.AccumLoss()
        t_3d = loss_utils.AccumLoss()

        self.model.eval()

        dim_used = self.train_dataset.dim_used
        input_frames = self.config.trainer.input_frames
        dct_cofs = self.config.trainer.dct_cofs
        input_type = self.config.data.type

        for batch_num, (inputs, targets, all_seq) in enumerate(self.valid_loader):

            inputs = inputs.to(self.device, non_blocking=True).float()
            targets = targets.to(self.device, non_blocking=True).float()
            all_seq = all_seq.to(self.device, non_blocking=True).float()

            outputs = self.model(inputs)
            n = outputs.shape[0] # batch size

            if input_type == 'rot':
                # angle space error
                e_err = loss_utils.euler_error(outputs, all_seq, input_frames, dim_used, dct_cofs)
                t_e.update(e_err.cpu().data.numpy() * n, n)
                # 3d error
                m_err = loss_utils.mpjpe_error(outputs, all_seq, input_frames, dim_used, dct_cofs)
                t_3d.update(m_err.cpu().data.numpy() * n, n)
            else:
                m_err = loss_utils.mpjpe_error_p3d(outputs, all_seq, input_frames, dim_used, dct_cofs)
                t_3d.update(m_err.cpu().data.numpy() * n, n)

        val_dict = {}
        val_dict['eul'] = t_e.avg
        val_dict['p3d'] = t_3d.avg
        val_list = np.array([t_e.avg, t_3d.avg])
        # loss_list = [t_3d.avg]
        return val_dict


    def test(self, act):

        if self.config.trainer.output_frames >= 25:
            eval_frame = [1, 3, 7, 9, 13, 24] # 560, 1000
        elif self.config.trainer.output_frames == 10:
            eval_frame = [1, 3, 7, 9] # 80, 160, 320, 400
        
        t_e = np.zeros(len(eval_frame))
        t_3d = np.zeros(len(eval_frame))

        dim_used = self.train_dataset.dim_used
        input_frames = self.config.trainer.input_frames
        dct_cofs = self.config.trainer.dct_cofs
        input_type = self.config.data.type

        self.model.eval()
        for batch_num, (inputs, targets, all_seq) in enumerate(self.test_loader[act]):

            inputs = inputs.to(self.device, non_blocking=True).float()
            targets = targets.to(self.device, non_blocking=True).float()
            all_seq = all_seq.to(self.device, non_blocking=True).float()

            outputs = self.model(inputs)

            n, seq_len, dim_full_len = all_seq.shape

            if input_type == 'rot':
                pred_eul, targ_eul = loss_utils.get_eul_sequence(outputs, all_seq, dim_used, dct_cofs)
                pred_eul = pred_eul[:, input_frames:, :, :].view(n, -1, dim_full_len)
                targ_eul = targ_eul[:, input_frames:, :, :].view(n, -1, dim_full_len)

                pred_p3d, targ_p3d = loss_utils.get_p3d_sequence(outputs, all_seq, dim_used, dct_cofs)
                pred_p3d = pred_p3d[:, input_frames:, :, :]
                targ_p3d = targ_p3d[:, input_frames:, :, :]
            else:
                outputs_3d = loss_utils.reverse_dct(outputs, seq_len, len(dim_used), dct_cofs)
                pred_3d = all_seq.clone()
                dim_used = np.array(dim_used)

                # joints at same loc
                joint_to_ignore = np.array([16, 20, 23, 24, 28, 31])
                index_to_ignore = np.concatenate((joint_to_ignore * 3, joint_to_ignore * 3 + 1, joint_to_ignore * 3 + 2))
                joint_equal = np.array([13, 19, 22, 13, 27, 30])
                index_to_equal = np.concatenate((joint_equal * 3, joint_equal * 3 + 1, joint_equal * 3 + 2))

                pred_3d[:, :, dim_used] = outputs_3d
                pred_3d[:, :, index_to_ignore] = pred_3d[:, :, index_to_equal]
                pred_p3d = pred_3d.contiguous().view(n, seq_len, -1, 3)[:, input_frames:, :, :]
                targ_p3d = all_seq.contiguous().view(n, seq_len, -1, 3)[:, input_frames:, :, :]
            
            for i, frame in enumerate(eval_frame):

                if input_type == 'rot':
                    loss_eul = torch.mean(torch.norm(pred_eul[:, frame, :] - targ_eul[:, frame, :], 2, 1))
                    t_e[i] += loss_eul.cpu().data.numpy()

                loss_p3d = torch.mean(torch.norm(pred_p3d[:, frame, :, :] - targ_p3d[:, frame, :, :], 2, 2))
                t_3d[i] += loss_p3d.cpu().data.numpy()

        test_dict = {}
        test_dict['eul'] = t_e
        test_dict['p3d'] = t_3d
        return test_dict


    def set_logger(self, log_file):
        self.logger.setLevel(logging.DEBUG)
        formatter = logging.Formatter(fmt='%(asctime)s: %(message)s')
        stream_handler = logging.StreamHandler()
        stream_handler.setLevel(logging.INFO)
        stream_handler.setFormatter(formatter)
        self.logger.addHandler(stream_handler)
        file_handler = logging.FileHandler(log_file)
        file_handler.setLevel(logging.INFO)
        file_handler.setFormatter(formatter)
        self.logger.addHandler(file_handler) 

def parse_args():
    parser = argparse.ArgumentParser(description='Train a motion generator')
    parser.add_argument(
        'config',
        help='train config file path')
    parser.add_argument(
        '--resume_from',
        help='the checkpoint file to resume from')
    args = parser.parse_args()
    return args

def main():
    # old version
    args = parse_args()
    cfg = Dict(yaml.safe_load(open(args.config)))
    if args.resume_from is not None:
        cfg.resume_from = args.resume_from
    trainer = Trainer(cfg)
    trainer.run()

    # args_path = './experiments/trainer/train_tn.yml'
    # cfg = Dict(yaml.safe_load(open(args_path)))

    # trainer = Trainer(cfg)
    # trainer.run()

if __name__ == '__main__':
    main()
