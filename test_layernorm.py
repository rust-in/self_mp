import torch
import numpy as np
import torch.nn as nn

a = torch.Tensor(
    [
        [0, 1, 2, 3],
        [0, 1, 2, 3],
        [0, 1, 2, 3]
    ]
).float()

a = a.repeat(5, 4, 1, 1)

b = torch.Tensor(
    [
        [0, 1, 2, 3],
        [6, 5, 4, 3],
        [0, 1, 2, 3]
    ]
).float()

b = b.repeat(5, 4, 1, 1)

layer = nn.LayerNorm([3, 4])

print(layer(b))