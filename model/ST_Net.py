import torch
import torch.nn as nn
import torch.functional as F
import numpy as np
from torch.nn import init
from torch.nn.parameter import Parameter
import math

from .attention import ST_Attention

def init_function():
    return init.kaiming_uniform_
    # return lambda x: x


def get_subsequent_mask(seq_len=100):
    ''' For masking out the subsequent info. '''
    subsequent_mask = (1 - torch.triu(
        torch.ones((1, seq_len, seq_len)), diagonal=1)).bool()
    return subsequent_mask

def get_positional_encoding(d_model, max_len=5000):
    pe = torch.zeros(max_len, d_model)
    position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)
    div_term = torch.exp(torch.arange(0, d_model, 2).float() * (-math.log(10000.0) / d_model))
    pe[:, 0::2] = torch.sin(position * div_term)
    pe[:, 1::2] = torch.cos(position * div_term)
    pe = pe.transpose(0, 1).unsqueeze(0).unsqueeze(0)

    return pe.transpose(-1, -2)


class ST_Net(nn.Module):
    def __init__(self, n_joint, n_dim, n_embed, n_head, d_k, d_v, d_feed, n_atten, dropout=0.1):

        super(ST_Net, self).__init__()

        self.n_embde = n_embed
        self.pos_encoding = get_positional_encoding(n_embed)
        self.mask = get_subsequent_mask()

        self.joint_embed = Parameter(torch.FloatTensor(n_joint, n_dim, n_embed))
        self.dropout_after_pos_encoding = nn.Dropout(dropout)

        self.attention_stack = nn.ModuleList([
            ST_Attention(n_node=n_joint, d_model=n_embed, n_head=n_head, d_k=d_k,
                         d_v=d_v, d_feed=d_feed, dropout=dropout)
            for _ in range(n_atten)
        ])
        self.joint_unembed = Parameter(torch.FloatTensor(n_joint, n_embed, n_dim))
        
        init_function()(self.joint_embed)
        init_function()(self.joint_unembed)

    def forward(self, input):
        _, _, seq_len, _ = input.shape
        # pos_encoding = get_positional_encoding(seq_len, self.n_embde, input.device)
        pos_encoding = self.pos_encoding[..., :seq_len, :].to(input.device)
        mask = self.mask[..., :seq_len, :seq_len].to(input.device)

        j_embed = torch.matmul(input, self.joint_embed)
        # x = pos_encoder(j_embed)

        x = self.dropout_after_pos_encoding(j_embed + pos_encoding)
        for attention_layer in self.attention_stack:
            x = attention_layer(x, mask)
        j_unembed = torch.matmul(x, self.joint_unembed)
        output = input + j_unembed
        return output


