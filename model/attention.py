import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.parameter import Parameter
from torch.nn import init

def init_function():
    return init.kaiming_uniform_
    # return lambda x: x

class ScaledDotProductAttention(nn.Module):
    ''' Scaled Dot-Product Attention '''

    def __init__(self, dropout=0.1):
        super().__init__()
        self.dropout = nn.Dropout(dropout)

    def forward(self, q, k, v, scale=None, mask=None):
        # input shape: b x N x H x T x F

        attn = torch.matmul(q, k.transpose(-1, -2))

        if scale is not None:
            attn = attn * scale

        if mask is not None:
            attn = attn.masked_fill(mask == 0, -1e9)

        attn = self.dropout(F.softmax(attn, dim=-1))
        output = torch.matmul(attn, v)

        return output, attn


class MultiHeadTemporalAttention(nn.Module):
    ''' Multi-Head Temporal Attention module '''

    def __init__(self, n_node, d_model, n_head, d_k, d_v, dropout=0.1):
        super().__init__()

        self.n_head = n_head
        self.d_k = d_k
        self.d_v = d_v
        self.d_model = d_model

        self.w_qs = Parameter(torch.FloatTensor(n_node, d_model, n_head * d_k)) 
        self.w_ks = Parameter(torch.FloatTensor(n_node, d_model, n_head * d_k))
        self.w_vs = Parameter(torch.FloatTensor(n_node, d_model, n_head * d_v))
        self.w_fcs = Parameter(torch.FloatTensor(n_node, n_head * d_v, d_model))

        init_function()(self.w_qs)
        init_function()(self.w_ks)
        init_function()(self.w_vs)
        init_function()(self.w_fcs)

        self.attention = ScaledDotProductAttention(dropout=0)

        self.dropout = nn.Dropout(dropout)
        self.layer_norm = nn.LayerNorm(d_model, eps=1e-6)

    def forward(self, q, k, v, mask=None):
        
        d_k, d_v, n_head, d_model = self.d_k, self.d_v, self.n_head, self.d_model

        residual = q

        # Separate different heads: b x N x T x H x F
        q = torch.matmul(q, self.w_qs).view(q.shape[:-1] + (n_head, d_k))
        k = torch.matmul(k, self.w_ks).view(k.shape[:-1] + (n_head, d_k))
        v = torch.matmul(v, self.w_vs).view(v.shape[:-1] + (n_head, d_v))

        # Transpose for attention dot product: b x N x H x T x F
        q, k, v = q.transpose(-2, -3), k.transpose(-2, -3), v.transpose(-2, -3)

        if mask is not None:
            mask = mask.unsqueeze(-3)   # For head axis broadcasting.

        q, attn = self.attention(q, k, v, scale=1/d_model**0.5, mask=mask)

        # Transpose to move the head dimension back: b x N x T x H x F
        # Combine the last two dimensions to concatenate all the heads together: b x N x T x (HxF)
        q = q.transpose(-2, -3).contiguous()
        q = q.view(q.shape[:-2] + (-1,))
        q = self.dropout(torch.matmul(q, self.w_fcs))
        q += residual

        q = self.layer_norm(q)

        return q, attn


class MultiHeadSpatialAttention(nn.Module):
    ''' Multi-Head Spatial Attention module '''

    def __init__(self, n_node, d_model, n_head, d_k, d_v, dropout=0.1):
        super().__init__()

        self.n_head = n_head
        self.d_k = d_k
        self.d_v = d_v
        self.d_model = d_model

        self.w_qs = Parameter(torch.FloatTensor(n_node, n_head * d_k, d_model)) 
        self.w_ks = Parameter(torch.FloatTensor(d_model, n_head * d_k))
        self.w_vs = Parameter(torch.FloatTensor(d_model, n_head * d_v))
        self.w_fcs = Parameter(torch.FloatTensor(n_head * d_v, d_model))

        init_function()(self.w_qs)
        init_function()(self.w_ks)
        init_function()(self.w_vs)
        init_function()(self.w_fcs)

        self.attention = ScaledDotProductAttention(dropout=0)

        self.dropout = nn.Dropout(dropout)
        self.layer_norm = nn.LayerNorm(d_model, eps=1e-6)

    def forward(self, q, k, v, mask=None):
        
        d_k, d_v, n_head, d_model = self.d_k, self.d_v, self.n_head, self.d_model

        residual = q

        # Separate different heads: b x T x N x H x F
        q = torch.matmul(self.w_qs, q.unsqueeze(-1)).squeeze(-1).view(q.shape[:-1] + (n_head, d_k))
        k = torch.matmul(k, self.w_ks).view(k.shape[:-1] + (n_head, d_k))
        v = torch.matmul(v, self.w_vs).view(v.shape[:-1] + (n_head, d_v))

        # Transpose for attention dot product: b x T x H x N x F
        q, k, v = q.transpose(-2, -3), k.transpose(-2, -3), v.transpose(-2, -3)

        if mask is not None:
            mask = mask.unsqueeze(-3)   # For head axis broadcasting.

        q, attn = self.attention(q, k, v, scale=1/d_model**0.5, mask=mask)

        # Transpose to move the head dimension back: b x T x N x H x F
        # Combine the last two dimensions to concatenate all the heads together: b x T x N x (HxF)
        q = q.transpose(-2, -3).contiguous()
        q = q.view(q.shape[:-2] + (-1,))
        q = self.dropout(torch.matmul(q, self.w_fcs))
        q += residual

        q = self.layer_norm(q)

        return q, attn


class ST_Attention(nn.Module):

    def __init__(self, n_node, d_model, n_head, d_k, d_v, d_feed, dropout):
        super(ST_Attention, self).__init__()
        self.temporal_trans = MultiHeadTemporalAttention(n_node=n_node, d_model=d_model, n_head=n_head,
                                                         d_k=d_k, d_v=d_v, dropout=dropout)
        self.spatial_trans = MultiHeadSpatialAttention(n_node=n_node, d_model=d_model, n_head=n_head,
                                                       d_k=d_k, d_v=d_v, dropout=dropout)
        self.feed_forward = nn.Sequential(
            nn.Linear(d_model, d_feed),
            nn.ReLU(),
            nn.Linear(d_feed, d_model),
            nn.Dropout(dropout, inplace=True)
        )
        self.layer_norm = nn.LayerNorm(d_model)

    def forward(self, x, mask):
        
        temporal_output, _ = self.temporal_trans(x, x, x, mask)
        x = x.transpose(1, 2) 
        spatial_output, _ = self.spatial_trans(x, x, x)
        
        residual = temporal_output + spatial_output.transpose(1,2)

        output = residual + self.feed_forward(residual)
        output = self.layer_norm(output)

        return output