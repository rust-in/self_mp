#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import print_function

import torch.nn as nn
import torch
from torch.nn.parameter import Parameter
import math


class GraphConvolution1(nn.Module):
    """
    adapted from : https://github.com/tkipf/gcn/blob/92600c39797c2bfb61a508e52b88fb554df30177/gcn/layers.py#L132
    """

    def __init__(self, in_features, out_features, bias=True, node_n=48, expert_model_num=4):
        super(GraphConvolution1, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.weight = Parameter(torch.FloatTensor(expert_model_num, in_features, out_features))
        self.att = Parameter(torch.FloatTensor(node_n, node_n))
        if bias:
            self.bias = Parameter(torch.FloatTensor(expert_model_num, out_features))
            self.test = Parameter(torch.FloatTensor(out_features))
        else:
            self.register_parameter('bias', None)
        self.reset_parameters()

    def reset_parameters(self):
        stdv = 1. / math.sqrt(self.weight.size(1))
        self.weight.data.uniform_(-stdv, stdv)
        self.att.data.uniform_(-stdv, stdv)
        if self.bias is not None:
            self.bias.data.uniform_(-stdv, stdv)

    def forward(self, input, coeff):

        coeff_w = coeff.unsqueeze(2).unsqueeze(2)
        coeff_b = coeff.unsqueeze(2)
        W = torch.sum(coeff_w * self.weight.unsqueeze(0), dim=1)
        b = torch.sum(coeff_b * self.bias.unsqueeze(0), dim=1)

        support = torch.matmul(input, W)
        output = torch.matmul(self.att, support)

        if self.bias is not None:
            return output + b.unsqueeze(1)
        else:
            return output

    def __repr__(self):
        return self.__class__.__name__ + ' (' \
               + str(self.in_features) + ' -> ' \
               + str(self.out_features) + ')'


class GraphConvolution2(nn.Module):
    """
    adapted from : https://github.com/tkipf/gcn/blob/92600c39797c2bfb61a508e52b88fb554df30177/gcn/layers.py#L132
    """

    def __init__(self, in_features, out_features, bias=True, node_n=48, expert_model_num=4):
        super(GraphConvolution2, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.weight = Parameter(torch.FloatTensor(in_features, out_features))
        self.att = Parameter(torch.FloatTensor(expert_model_num, node_n, node_n))
        if bias:
            self.bias = Parameter(torch.FloatTensor(expert_model_num, out_features))
        else:
            self.register_parameter('bias', None)
        self.reset_parameters()

    def reset_parameters(self):
        stdv = 1. / math.sqrt(self.weight.size(1))
        self.weight.data.uniform_(-stdv, stdv)
        self.att.data.uniform_(-stdv, stdv)
        if self.bias is not None:
            self.bias.data.uniform_(-stdv, stdv)

    def forward(self, input, coeff):
        support = torch.matmul(input, self.weight)

        coeff_w = coeff.unsqueeze(2).unsqueeze(2)
        coeff_b = coeff.unsqueeze(2)
        A = torch.sum(coeff_w * self.att.unsqueeze(0), dim=1)
        b = torch.sum(coeff_b * self.bias.unsqueeze(0), dim=1) 

        output = torch.matmul(A, support)
        if self.bias is not None:
            return output + b.unsqueeze(1)
        else:
            return output

    def __repr__(self):
        return self.__class__.__name__ + ' (' \
               + str(self.in_features) + ' -> ' \
               + str(self.out_features) + ')'


class GraphConvolution3(nn.Module):
    """
    adapted from : https://github.com/tkipf/gcn/blob/92600c39797c2bfb61a508e52b88fb554df30177/gcn/layers.py#L132
    """

    def __init__(self, in_features, out_features, bias=True, node_n=48, expert_model_num=4):
        super(GraphConvolution3, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.weight = Parameter(torch.FloatTensor(expert_model_num, in_features, out_features))
        self.att = Parameter(torch.FloatTensor(expert_model_num, node_n, node_n))
        if bias:
            self.bias = Parameter(torch.FloatTensor(expert_model_num, out_features))
        else:
            self.register_parameter('bias', None)
        self.reset_parameters()

    def reset_parameters(self):
        stdv = 1. / math.sqrt(self.weight.size(1))
        self.weight.data.uniform_(-stdv, stdv)
        self.att.data.uniform_(-stdv, stdv)
        if self.bias is not None:
            self.bias.data.uniform_(-stdv, stdv)

    def forward(self, input, coeff):

        coeff_w = coeff.unsqueeze(2).unsqueeze(2)
        coeff_b = coeff.unsqueeze(2)
        W = torch.sum(coeff_w * self.weight.unsqueeze(0), dim=1)
        A = torch.sum(coeff_w * self.att.unsqueeze(0), dim=1)
        b = torch.sum(coeff_b * self.bias.unsqueeze(0), dim=1) 

        support = torch.matmul(input, W)
        output = torch.matmul(A, support)
        if self.bias is not None:
            return output + b.unsqueeze(1)
        else:
            return output

    def __repr__(self):
        return self.__class__.__name__ + ' (' \
               + str(self.in_features) + ' -> ' \
               + str(self.out_features) + ')'

def select_gcn_type(gcn_type):
    # 0 is origin struct
    if gcn_type == 1:
        return GraphConvolution1
    elif gcn_type == 2:
        return GraphConvolution2
    else:
        return GraphConvolution3

def select_norm_type(norm_type, feature_dim, input_dim):
    if norm_type == 1:
        return nn.InstanceNorm1d(feature_dim), nn.InstanceNorm2d(feature_dim)
    elif norm_type == 2:
        return nn.LayerNorm(feature_dim), nn.LayerNorm([feature_dim] + input_dim)
    else: # 0 or other
        return nn.BatchNorm1d(feature_dim), nn.BatchNorm2d(feature_dim)


class GC_Block(nn.Module):
    def __init__(self, in_features, p_dropout, bias=True, node_n=48, gcn_type=1, norm_type=0, expert_model_num=4):
        """
        Define a residual block of GCN
        """
        super(GC_Block, self).__init__()
        self.in_features = in_features
        self.out_features = in_features

        GraphConvolution = select_gcn_type(gcn_type)
        self.gc1 = GraphConvolution(in_features, in_features, node_n=node_n, bias=bias, expert_model_num=expert_model_num)
        self.bn1 = select_norm_type(norm_type, in_features, [])[0]
        # self.bn1 = nn.BatchNorm1d(node_n * in_features)

        self.gc2 = GraphConvolution(in_features, in_features, node_n=node_n, bias=bias, expert_model_num=expert_model_num)
        self.bn2 = select_norm_type(norm_type, in_features, [])[0]
        # self.bn2 = nn.BatchNorm1d(node_n * in_features)

        self.do = nn.Dropout(p_dropout)
        self.act_f = nn.Tanh()

    def forward(self, x, coeff):
        y = self.gc1(x, coeff)
        b, n, f = y.shape
        # y = self.bn1(y.view(b, -1)).view(b, n, f)
        y = self.bn1(y)
        y = self.act_f(y)
        y = self.do(y)

        y = self.gc2(y, coeff)
        b, n, f = y.shape
        # y = self.bn2(y.view(b, -1)).view(b, n, f)
        y = self.bn2(y)
        y = self.act_f(y)
        y = self.do(y)

        return y + x

    def __repr__(self):
        return self.__class__.__name__ + ' (' \
               + str(self.in_features) + ' -> ' \
               + str(self.out_features) + ')'


class Gating(nn.Module):
    def __init__(self, node, input_feature, norm_type=0, expert_model_num=4):
        super().__init__()
        self.output_shape = expert_model_num

        node_dim = int((node - 7) / 2) + 1
        feature_dim = int((input_feature - 2) / 2) + 1
        self.layer1 = nn.Sequential(
            nn.Conv2d(1, 64, (7,2), stride=(2,2)),
            select_norm_type(norm_type, 64, [node_dim, feature_dim])[1],
            # nn.BatchNorm2d(64),
            nn.LeakyReLU(inplace=True)
        )

        node_dim = int((node_dim - 7) / 2) + 1
        feature_dim = int((feature_dim - 2) / 2) + 1
        self.layer2 = nn.Sequential(
            nn.Conv2d(64, 128, (7,2), stride=(2,2)),
            select_norm_type(norm_type, 128, [node_dim, feature_dim])[1],
            # nn.BatchNorm2d(128),
            nn.LeakyReLU(inplace=True)
        )

        self.layer3 = nn.Sequential(
            nn.Linear(128*node_dim*feature_dim, 128),
            # select_norm_type(norm_type, 128, [])[0],
            # nn.BatchNorm1d(128),
            nn.LeakyReLU(),
            nn.Dropout(0.5, inplace=True)
        )
        self.fc_out = nn.Linear(128, self.output_shape)
        self.unify = nn.Softmax(dim=1)
    
    def forward(self, input):
        x = self.layer1(input.unsqueeze(1))
        x = self.layer2(x)
        x = x.view(x.size(0), -1)
        x = self.layer3(x)
        y = self.unify(self.fc_out(x))
        return y


class GCN_mul(nn.Module):
    def __init__(self, input_feature, hidden_feature, p_dropout, num_stage=1, node_n=48, expert_model_num=4, gcn_type=1, norm_type=0):
        """

        :param input_feature: num of input feature
        :param hidden_feature: num of hidden feature
        :param p_dropout: drop out prob.
        :param num_stage: number of residual blocks
        :param node_n: number of nodes in graph
        """


        super(GCN_mul, self).__init__()
        self.num_stage = num_stage

        self.gatingNN = Gating(node_n, input_feature, norm_type=norm_type, expert_model_num=expert_model_num)

        GraphConvolution = select_gcn_type(gcn_type)
        self.gc1 = GraphConvolution(input_feature, hidden_feature, node_n=node_n, expert_model_num=expert_model_num)
        self.bn1 = select_norm_type(norm_type, hidden_feature, [])[0]
        # self.bn1 = nn.BatchNorm1d(node_n * hidden_feature)

        self.gcbs = []
        for i in range(num_stage):
            self.gcbs.append(GC_Block(hidden_feature, p_dropout=p_dropout, node_n=node_n, gcn_type=gcn_type, norm_type=norm_type,
                                      expert_model_num=expert_model_num))

        self.gcbs = nn.ModuleList(self.gcbs)

        self.gc7 = GraphConvolution(hidden_feature, input_feature, node_n=node_n, expert_model_num=expert_model_num)

        self.do = nn.Dropout(p_dropout)
        self.act_f = nn.Tanh()

    def forward(self, x):

        coeff = self.gatingNN(x)

        y = self.gc1(x, coeff)
        b, n, f = y.shape
        # y = self.bn1(y.view(b, -1)).view(b, n, f)
        y = self.bn1(y)
        y = self.act_f(y)
        y = self.do(y)

        for i in range(self.num_stage):
            y = self.gcbs[i](y, coeff)

        y = self.gc7(y, coeff)
        y = y + x

        return y
