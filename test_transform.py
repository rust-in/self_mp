import os
import argparse
import json
import logging
import time
import yaml
import pickle as pk
import numpy as np
from addict import Dict
import copy
import pandas as pd

import torch
from torch import nn
from torch.autograd import Variable
from torch.utils.data import Subset
import torch.nn.functional as F
from torch.utils.tensorboard import SummaryWriter
from model import ST_Net, get_positional_encoding, get_subsequent_mask
from data_loader import H36MDataset, H36MTransformDataset, build_dataloader
from data_loader.settings import define_actions_h36m, define_actions_cmu
from utils import trainer_utils, loss_utils

import datetime


data_dir = './data/'
name = 'transform'
input_type = 'rotmat'
input_n = 50
output_n = 100
sample_rate = 2
train_batch = 16
test_batch = 128
job = 10

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

model = ST_Net(n_joint=31, n_dim=9, n_embed=64, n_head=4, d_k=16, d_v=16, d_feed=128, n_atten=4, dropout=0.1)
model.to(device)
model = nn.DataParallel(model)
model.train()

# pos_encoding = get_positional_encoding(75, 64, device)
# pos_encoding = nn.DataParallel(pos_encoding)

# print('start', datetime.datetime.now())
# train_dataset = H36MTransformDataset(path=data_dir, name=name, input_type=input_type, actions='all', 
#                                      input_frames=input_n, output_frames=output_n, split=2, sample_rate=2)

# print('end', datetime.datetime.now())
# train_loader = build_dataloader(train_dataset, batch_size=train_batch, shuffle=True, pin_memory=True)


test_loader = dict()
acts = define_actions_h36m('all')
for act in acts:
    test_dataset = H36MTransformDataset(path=data_dir, name=name, input_type=input_type, actions=act, 
                                        input_frames=input_n, output_frames=output_n, split=1)


    # inputs = inputs.to(device).float()
    # output = model(inputs, pos_encoding, mask)
