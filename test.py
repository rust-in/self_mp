import numpy as np
import torch
import math
from utils import data_utils
from utils.Quaternions import Quaternions
from scipy.spatial.transform import Rotation as R

device = torch.device(
    'cuda:0' if torch.cuda.is_available() else 'cpu')

from scipy.linalg import sqrtm, inv

def sym(w):
    return w.dot(inv(sqrtm(w.T.dot(w))))

def gs_cofficient(v1, v2):
    return np.dot(v2, v1) / np.dot(v1, v1)

def multiply(cofficient, v):
    return map((lambda x : x * cofficient), v)

def proj(v1, v2):
    return multiply(gs_cofficient(v1, v2) , v1)

def gs(X):
    Y = []
    for i in range(len(X)):
        temp_vec = X[i]
        for inY in Y :
            proj_vec = proj(inY, X[i])
            #print "i =", i, ", projection vector =", proj_vec
            temp_vec = map(lambda x, y : x - y, temp_vec, proj_vec)
            #print "i =", i, ", temporary vector =", temp_vec
        Y.append(temp_vec)
    return Y
# a = torch.arange(5*93).float()
# b = torch.arange(5*93).float() + 1

# a = a.view(5, -1, 3).to(device)
# b = b.view(5, -1, 3).to(device)

# err = torch.mean(torch.norm(a-b, 2, -1))
# print(err)

# c = torch.zeros((5,1,3)).float().to(a.device)
# a = torch.cat((c,a),1)
# b = torch.cat((c,b),1)

# err = torch.mean(torch.norm(a-b, 2, -1))
# print(err)

# a = torch.arange(5*31*50*9)
# a = a.view(5, 31, 50, 9)
# a = a.view(*a.shape[:-1], 3, 3).to(device).float()

# b = torch.arange(5*31*50*9) + 1
# b = b.view(5, 31, 50, 9)
# b = b.view(*b.shape[:-1], 3, 3).to(device).float()

# err = torch.mean(torch.norm(a-b, 2, -1))

# print(err)

# a = a.view(-1, 3)
# b = b.view(-1, 3)

# err_a = torch.mean(torch.norm(a-b, 2, -1))
# print(err_a)

# top = torch.zeros(3)
# top = top[(None,) * 3 + (...,)].expand(16, 2, 50, -1)

# a = torch.ones((16, 31, 50, 3))

# b = torch.cat((top, a), 1)
# print(b.shape)
# print(b[:, :2, :, :])

# a = torch.Tensor([1])

# print(torch.asin(a))

# device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
# a = torch.Tensor([[1,2,3]]).to(device).float()
# r = data_utils.expmap2rotmat_torch(a)
# print(r)


# device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

# a = torch.Tensor(
#     [
#         [0, 1, 0],
#         [0, 0, 1],
#         [0, 0, -1]
#     ]
# ).to(device).float()
# a = a.repeat(2, 2, 3, 1, 1)

# up = a.transpose(-1, -2) - a

# trace = a[..., 0, 0] + a[..., 1, 1] + a[..., 2, 2]
# down = 1 + trace
# down = down.unsqueeze(-1).unsqueeze(-1)

# before = torch.div(up, down)

# eye = torch.eye(3).to(device)
# r = torch.matmul((eye + before).inverse(), eye-before)

# a = np.array(
#     [
#         [0, 1, 0],
#         [0, 0, 1],
#         [0, 0, -1]
#     ]
# )

# test = gs(a)
# print(test)


"""
x_new = 1/2*(3-dot(x_ort,x_ort))*x_ort
y_new = 1/2*(3-dot(y_ort,y_ort))*y_ort
z_new = 1/2*(3-dot(z_ort,z_ort))*z_ort
"""
data_idx = []

valid_frames = np.arange(0, 1000-75, 5)

idx_1 = [(1, 'walking', 1)] * len(valid_frames)
idx_2 = list(valid_frames)

data_idx.extend(zip(idx_1, idx_2))

print(data_idx)
