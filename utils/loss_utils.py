#!/usr/bin/env python
# -*- coding: utf-8 -*-
import torch
import os
import numpy as np
from torch.autograd import Variable
from utils import data_utils

"""
adapted from
https://github.com/wei-mao-2019/LearnTrajDep/blob/master/utils/utils.py
"""
class AccumLoss(object):
    def __init__(self, step=0):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0
        self.step = step

    def update(self, val, n=1):
        self.val = val
        self.sum += val
        self.count += n
        self.step += 1
        self.avg = self.sum / self.count


def reverse_dct(outputs, seq_len, dim_used_len, dct_n):
    # inverse dct transformation
    _, idct_m = data_utils.get_dct_matrix(seq_len)
    idct_m = Variable(torch.from_numpy(idct_m)).float().cuda()
    outputs_t = outputs.view(-1, dct_n).transpose(0, 1)
    outputs_reverse = torch.matmul(idct_m[:, :dct_n], outputs_t).transpose(0, 1).contiguous().view(-1, dim_used_len,
                                                                                               seq_len).transpose(1, 2)
    return outputs_reverse


def get_eul_sequence(outputs, all_seq, dim_used, dct_n):
    n, seq_len, dim_full_len = all_seq.data.shape

    # inverse dct transformation
    outputs_exp = reverse_dct(outputs, seq_len, len(dim_used), dct_n)

    pred_expmap = all_seq.clone()
    dim_used = np.array(dim_used)
    pred_expmap[:, :, dim_used] = outputs_exp

    pred_expmap = pred_expmap.contiguous().view(-1, dim_full_len)
    targ_expmap = all_seq.clone().contiguous().view(-1, dim_full_len)

    pred_expmap[:, 0:6] = 0
    targ_expmap[:, 0:6] = 0
    pred_expmap = pred_expmap.view(-1, 3)
    targ_expmap = targ_expmap.view(-1, 3)

    pred_eul = data_utils.rotmat2euler_torch(data_utils.expmap2rotmat_torch(pred_expmap))
    pred_eul = pred_eul.view(n, seq_len, -1, 3)

    targ_eul = data_utils.rotmat2euler_torch(data_utils.expmap2rotmat_torch(targ_expmap))
    targ_eul = targ_eul.view(n, seq_len, -1, 3)

    return pred_eul, targ_eul    
    

def get_p3d_sequence(outputs, all_seq, dim_used, dct_n):
    n, seq_len, dim_full_len = all_seq.data.shape

    # inverse dct transformation
    outputs_exp = reverse_dct(outputs, seq_len, len(dim_used), dct_n)

    pred_expmap = all_seq.clone()
    dim_used = np.array(dim_used)
    pred_expmap[:, :, dim_used] = outputs_exp
    pred_expmap = pred_expmap.contiguous().view(-1, dim_full_len).clone()
    targ_expmap = all_seq.clone().contiguous().view(-1, dim_full_len)

    pred_expmap[:, 0:6] = 0
    targ_expmap[:, 0:6] = 0

    targ_p3d = data_utils.expmap2xyz_torch(targ_expmap).view(n, seq_len, -1, 3)

    pred_p3d = data_utils.expmap2xyz_torch(pred_expmap).view(n, seq_len, -1, 3)

    return pred_p3d, targ_p3d

def sen_loss(outputs, all_seq, dim_used, dct_n):
    """

    :param outputs: N * 48 * dct_n
    :param all_seq: N * seq_len * 99
    :param input_n:
    :param dim_used:
    :return:
    """
    n, seq_len, dim_full_len = all_seq.data.shape
    dim_used = np.array(dim_used)

    pred_expmap = reverse_dct(outputs, seq_len, len(dim_used), dct_n)
    targ_expmap = all_seq.clone()[:, :, dim_used]

    loss = torch.mean(torch.sum(torch.abs(pred_expmap - targ_expmap), dim=2).view(-1))

    return loss

def sen_loss_p3d(outputs, all_seq, dim_used, dct_n):
    """

    :param outputs:n*66*dct_n
    :param all_seq:
    :param dct_n:
    :param dim_used:
    :return:
    """
    n, seq_len, dim_full_len = all_seq.data.shape
    dim_used = np.array(dim_used)

    outputs_p3d = reverse_dct(outputs, seq_len, len(dim_used), dct_n)
    pred_3d = outputs_p3d.contiguous().view(-1, len(dim_used)).view(-1, 3)
    targ_3d = all_seq[:, :, dim_used].contiguous().view(-1, len(dim_used)).view(-1, 3)

    loss = torch.mean(torch.norm(pred_3d - targ_3d, 2, 1))

    return loss


def euler_error(outputs, all_seq, input_n, dim_used, dct_n):
    """

    :param outputs: N * 48 * dct_n
    :param all_seq: N * seq_len * 99
    :param input_n:
    :param dim_used:
    :return:
    """
    n, seq_len, dim_full_len = all_seq.data.shape
    pred_eul, targ_eul = get_eul_sequence(outputs, all_seq, dim_used, dct_n)
    pred_eul = pred_eul[:, input_n:, :, :].contiguous().view(-1, dim_full_len)
    targ_eul = targ_eul[:, input_n:, :, :].contiguous().view(-1, dim_full_len)
    mean_errors = torch.mean(torch.norm(pred_eul - targ_eul, 2, 1))

    return mean_errors


def mpjpe_error(outputs, all_seq, input_n, dim_used, dct_n):
    """

    :param outputs: N * 48 * dct_n
    :param all_seq: N * seq_len * 99
    :param input_n:
    :param dim_used:
    :param data_mean:
    :param data_std:
    :return:
    """
    pred_p3d, targ_p3d = get_p3d_sequence(outputs, all_seq, dim_used, dct_n)
    pred_p3d = pred_p3d[:, input_n:, :, :].contiguous().view(-1, 3)
    targ_p3d = targ_p3d[:, input_n:, :, :].contiguous().view(-1, 3)
    mean_3d_err = torch.mean(torch.norm(targ_p3d - pred_p3d, 2, 1))

    return mean_3d_err


def mpjpe_error_p3d(outputs, all_seq, input_n, dim_used, dct_n):
    """

    :param outputs:n*66*dct_n
    :param all_seq:
    :param dct_n:
    :param dim_used:
    :return:
    """
    n, seq_len, dim_full_len = all_seq.data.shape
    dim_used = np.array(dim_used)

    outputs_p3d = reverse_dct(outputs, seq_len, len(dim_used), dct_n)
    pred_3d = outputs_p3d[:, input_n:, :].contiguous().view(-1, len(dim_used)).view(-1, 3)
    targ_3d = all_seq[:, input_n:, dim_used].contiguous().view(-1, len(dim_used)).view(-1, 3)

    mean_3d_err = torch.mean(torch.norm(pred_3d - targ_3d, 2, 1))

    return mean_3d_err


def mpjpe_error_cmu(outputs, all_seq, input_n, dim_used, dct_n):
    n, seq_len, dim_full_len = all_seq.data.shape
    dim_used_len = len(dim_used)

    _, idct_m = data_utils.get_dct_matrix(seq_len)
    idct_m = Variable(torch.from_numpy(idct_m)).float().cuda()
    outputs_t = outputs.view(-1, dct_n).transpose(0, 1)
    outputs_exp = torch.matmul(idct_m[:, :dct_n], outputs_t).transpose(0, 1).contiguous().view(-1, dim_used_len,
                                                                                               seq_len).transpose(1, 2)
    pred_expmap = all_seq.clone()
    dim_used = np.array(dim_used)
    pred_expmap[:, :, dim_used] = outputs_exp
    pred_expmap = pred_expmap[:, input_n:, :].contiguous().view(-1, dim_full_len)
    targ_expmap = all_seq[:, input_n:, :].clone().contiguous().view(-1, dim_full_len)
    pred_expmap[:, 0:6] = 0
    targ_expmap[:, 0:6] = 0

    targ_p3d = data_utils.expmap2xyz_torch_cmu(targ_expmap).view(-1, 3)
    pred_p3d = data_utils.expmap2xyz_torch_cmu(pred_expmap).view(-1, 3)

    mean_3d_err = torch.mean(torch.norm(targ_p3d - pred_p3d, 2, 1))

    return mean_3d_err


def mpjpe_error_3dpw(outputs, all_seq, dct_n, dim_used):
    n, seq_len, dim_full_len = all_seq.data.shape

    _, idct_m = data_utils.get_dct_matrix(seq_len)
    idct_m = Variable(torch.from_numpy(idct_m)).float().cuda()
    outputs_t = outputs.view(-1, dct_n).transpose(0, 1)
    outputs_exp = torch.matmul(idct_m[:, 0:dct_n], outputs_t).transpose(0, 1).contiguous().view(-1, dim_full_len - 3,
                                                                                                seq_len).transpose(1,
                                                                                                                   2)
    pred_3d = all_seq.clone()
    pred_3d[:, :, dim_used] = outputs_exp
    pred_3d = pred_3d.contiguous().view(-1, dim_full_len).view(-1, 3)
    targ_3d = all_seq.contiguous().view(-1, dim_full_len).view(-1, 3)

    mean_3d_err = torch.mean(torch.norm(pred_3d - targ_3d, 2, 1))

    return mean_3d_err

def l1_loss(pred_seq, targ_seq):

    loss = torch.mean(torch.sum(torch.abs(pred_seq - targ_seq), dim=2))

    return loss


"""
transformer losses
"""

def l2_loss(pred_seq, targ_seq, dim=-1):
    """
    :param pred_seq: B * N * T * D
    :param targ_seq: B * N * T * D
    :param dim: norm dim
    :return:
    """

    loss = torch.mean(torch.sqrt(torch.sum((pred_seq - targ_seq) ** 2, dim=dim) + 1e-8))

    return loss

def l2_loss_rotmat_normalized(pred_seq, targ_seq, dim=-1):

    bs, joint_num, seq_len, dim_len = pred_seq.shape

    pred_seq = pred_seq.clone().view(bs, joint_num, seq_len, 3, 3)
    pred_seq = data_utils.rotmat_normalization_torch(pred_seq)
    pred_seq = pred_seq.view(bs, joint_num, seq_len, dim_len)


    loss = torch.mean(torch.sqrt(torch.sum((pred_seq - targ_seq) ** 2, dim=dim) + 1e-8))

    return loss


def l2_loss_rotmat2pos(pred_seq, targ_seq):
    """
    :param pred_seq: B * N * T * D=9
    :param targ_seq: B * N * T * D
    :return:
    """
    bs, joint_num, seq_len, dim_len = pred_seq.shape

    eye = torch.eye(3).to(pred_seq.device)
    eye = eye[(None,) * 3 + (...,)].expand(bs, -1, seq_len, -1, -1)

    pred_seq = pred_seq.view(*pred_seq.shape[:-1], 3, 3)
    pred_seq = data_utils.rotmat_normalization_torch(pred_seq)
    pred_seq = torch.cat((eye, pred_seq), 1).transpose(1,2).contiguous().view(-1, joint_num+1, 3, 3)
    pred_pos = data_utils.rotmat2xyz_torch(pred_seq).view(-1, (joint_num+1) * 3)

    targ_seq = targ_seq.view(*targ_seq.shape[:-1], 3, 3)
    targ_seq = torch.cat((eye, targ_seq), 1).transpose(1,2).contiguous().view(-1, joint_num+1, 3, 3)
    targ_pos = data_utils.rotmat2xyz_torch(targ_seq).view(-1, (joint_num+1) * 3)

    # loss = torch.mean(torch.norm(pred_pos - targ_pos, 2, -1))
    loss = torch.mean(torch.sqrt(torch.sum((pred_pos - targ_pos) ** 2, dim=-1) + 1e-8))
    return loss

def l2_loss_expmap2pos(pred_seq, targ_seq):
    """
    :param pred_seq: B * N * T * D=3
    :param targ_seq: B * N * T * D
    :return:
    """
    bs, joint_num, seq_len, dim_len = pred_seq.shape

    top = torch.zeros(3).to(pred_seq.device)
    top = top[(None,) * 3 + (...,)].expand(bs, 2, seq_len, -1)

    pred_seq = torch.cat((top, pred_seq), 1).transpose(1,2).contiguous().view(bs*seq_len, -1)
    pred_pos = data_utils.expmap2xyz_torch(pred_seq).view(-1, (joint_num+1) * 3)

    targ_seq = torch.cat((top, targ_seq), 1).transpose(1,2).contiguous().view(bs*seq_len, -1)
    targ_pos = data_utils.expmap2xyz_torch(targ_seq).view(-1, (joint_num+1) * 3)

    loss = torch.mean(torch.sqrt(torch.sum((pred_pos - targ_pos) ** 2, dim=-1) + 1e-8))
    return loss

def l2_loss_pos(pred_seq, targ_seq):

    bs, joint_num, seq_len, dim_len = pred_seq.shape

    pred_pos = pred_seq.transpose(1,2).contiguous().view(bs, seq_len, -1)
    targ_pos = targ_seq.transpose(1,2).contiguous().view(bs, seq_len, -1)

    loss = torch.mean(torch.sqrt(torch.sum((pred_pos - targ_pos) ** 2, dim=-1) + 1e-8))
    return loss


def euler_error_rotmat_transform(pred_seq, targ_seq):
    """
    :param pred_seq: B * N * T * D=9
    :param targ_seq: B * N * T * D
    :return:
    """

    bs, joint_num, seq_len, dim_len = pred_seq.shape

    pred_seq = pred_seq.clone().view(-1, 3, 3)
    pred_seq = data_utils.rotmat_normalization_torch(pred_seq)
    pred_eul = data_utils.rotmat2euler_torch(pred_seq)
    pred_eul = pred_eul.view(bs, joint_num, seq_len, 3).transpose(1,2)
    pred_eul = pred_eul.contiguous().view(-1, joint_num * 3)

    targ_seq = targ_seq.clone().view(-1, 3, 3)
    targ_eul = data_utils.rotmat2euler_torch(targ_seq)
    targ_eul = targ_eul.view(bs, joint_num, seq_len, 3).transpose(1,2)
    targ_eul = targ_eul.contiguous().view(-1, joint_num * 3)

    # eul_error = l2_loss(pred_eul, targ_eul)
    eul_error = torch.mean(torch.sqrt(torch.sum((pred_seq - targ_seq) ** 2, dim=-1) + 1e-8))

    return eul_error

def euler_error_expmap_transform(pred_seq, targ_seq):
    """
    :param pred_seq: B * N * T * D=3
    :param targ_seq: B * N * T * D
    :return:
    """
    bs, joint_num, seq_len, dim_len = pred_seq.shape

    pred_seq = pred_seq.transpose(1, 2).contiguous().view(-1, 3)
    pred_eul = data_utils.rotmat2euler_torch(data_utils.expmap2rotmat_torch(pred_seq))
    pred_eul = pred_eul.view(-1, joint_num*3)

    targ_seq = targ_seq.transpose(1, 2).contiguous().view(-1, 3)
    targ_eul = data_utils.rotmat2euler_torch(data_utils.expmap2rotmat_torch(targ_seq))
    targ_eul = targ_eul.view(-1, joint_num*3)

    eul_error = torch.mean(torch.sqrt(torch.sum((pred_seq - targ_seq) ** 2, dim=-1) + 1e-8))

    return eul_error
    
def mpjpe_error_rotmat_transform(pred_seq, targ_seq):
    """
    :param pred_seq: B * N * T * D (N=31, 32-1)
    :param targ_seq: B * N * T * D
    :return:
    """   

    bs, joint_num, seq_len, dim_len = pred_seq.shape

    eye = torch.eye(3).to(pred_seq.device).float()
    eye = eye[(None,) * 3 + (...,)].expand(bs, -1, seq_len, -1, -1)

    pred_seq = pred_seq.clone().view(*pred_seq.shape[:-1], 3, 3)
    pred_seq = data_utils.rotmat_normalization_torch(pred_seq)
    pred_seq = torch.cat((eye, pred_seq), 1).transpose(1,2).contiguous().view(-1, joint_num+1, 3, 3)
    pred_pos = data_utils.rotmat2xyz_torch(pred_seq)

    targ_seq = targ_seq.clone().view(*targ_seq.shape[:-1], 3, 3)
    targ_seq = torch.cat((eye, targ_seq), 1).transpose(1,2).contiguous().view(-1, joint_num+1, 3, 3)
    targ_pos = data_utils.rotmat2xyz_torch(targ_seq)

    # mpjpe_error = torch.mean(torch.norm(pred_pos - targ_pos, 2, -1))
    mpjpe_error = torch.mean(torch.sqrt(torch.sum((pred_pos - targ_pos) ** 2, dim=-1) + 1e-8))

    return mpjpe_error


def mpjpe_error_expmap_transform(pred_seq, targ_seq):
    """
    :param pred_seq: B * N * T * D (N=31, 32-1)
    :param targ_seq: B * N * T * D
    :return:
    """   

    bs, joint_num, seq_len, dim_len = pred_seq.shape

    top = torch.zeros(3).to(pred_seq.device)
    top = top[(None,) * 3 + (...,)].expand(bs, 2, seq_len, -1)

    pred_seq = torch.cat((top, pred_seq), 1).transpose(1,2).contiguous().view(bs*seq_len, -1)
    pred_pos = data_utils.expmap2xyz_torch(pred_seq)

    targ_seq = torch.cat((top, targ_seq), 1).transpose(1,2).contiguous().view(bs*seq_len, -1)
    targ_pos = data_utils.expmap2xyz_torch(targ_seq)

    mpjpe_error = torch.mean(torch.sqrt(torch.sum((pred_pos - targ_pos) ** 2, dim=-1) + 1e-8))

    return mpjpe_error

def mpjpe_error_pos_transform(pred_seq, targ_seq):

    bs, joint_num, seq_len, dim_len = pred_seq.shape

    top = torch.zeros(3).to(pred_seq.device)
    top = top[(None,) * 3 + (...,)].expand(bs, -1, seq_len, -1)

    pred_pos = torch.cat((top, pred_seq), 1) # ns, joint_num+1, seq_len, dim
    targ_pos = torch.cat((top, targ_seq), 1) 

    mpjpe_error = torch.mean(torch.sqrt(torch.sum((pred_pos - targ_pos) ** 2, dim=-1) + 1e-8))

    return mpjpe_error